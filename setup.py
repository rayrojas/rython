"""
 py2app/py2exe build script for MyApplication.

 Will automatically ensure that all build prerequisites are available
 via ez_setup

 Usage (Mac OS X):
	 python setup.py py2app

 Usage (Windows):
	 python setup.py py2exe
 """

from sys import platform, version_info

if version_info[0] != 2:
	print('ERROR: This is Python 2 script!')
	exit(1)

MAIN = './app/main.py'
NAME = "Rython"
VERSION = "0.0.1"

if platform == 'darwin':
	from setuptools import setup
	extra_options = dict(
		setup_requires=['py2app'],
		app=[MAIN],
		options=dict(
			py2app=dict(
				argv_emulation=True,
				iconfile='./docs/icons/rython.icns',
				includes=[],
				resources=[],
				plist=dict(
					CFBundleName=NAME,
					CFBundleShortVersionString=VERSION,
					CFBundleGetInfoString=NAME + " " + VERSION +
					", written 2016 by Ray Rojas",
					CFBundleExecutable=NAME,
					CFBundleIdentifier='info.equisd.rython',
					LSMinimumSystemVersion='10.8.0',
					NSHumanReadableCopyright='ISC License (ISCL)'
				)
			)
		)
	)

elif platform == 'win32':
	# noinspection PyUnresolvedReferences
	import py2exe
	import platform as arch
	from distutils.core import setup
	if arch.architecture()[0] == '64bit':
		suffix = '_64'
	else:
		suffix = ''
	additional_files = [('imageformats', []),
						('platforms', []),
						('', [])]
	extra_options = dict(
		options={'py2exe': {"bundle_files": 2,
							"dll_excludes": [],
							"dist_dir": "dist" + suffix,
							"compressed": True,
							"includes": [],
							"excludes": [],
							"optimize": 2}},
		windows=[{"script": MAIN,
				  "dest_base": "RYTHON",
				  "version": VERSION,
				  "copyright": "Ray Rojas 2016",
				  "legal_copyright": "ISC License (ISCL)",
				  "product_version": VERSION,
				  "product_name": "Rython tools for fun",
				  "file_description": "Rython tools for fun",
				  "icon_resources": [(1, ".\docs\icons\\rython.ico")]}],
		zipfile=None,
		data_files=additional_files)
else:
    print('Please use setup.sh to build Linux package.')
    exit()

setup(
    name=NAME,
    version=VERSION,
    author="Ray Rojas",
    author_email="me@rayrojas.info",
    description="Rython tools for fun",
    license="ISC License (ISCL)",
    keywords="Rython tools for fun",
    url="http://equisd.info/rython",
    **extra_options
)
