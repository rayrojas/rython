import wx
class Home(wx.Frame):
	def __init__(self, INFO, parent, _size = (390, 350)):
		super(Home, self).__init__(parent, title=INFO["title"], size=_size)
		
		panel = wx.Panel(self)

		font = wx.SystemSettings_GetFont(wx.SYS_SYSTEM_FONT)
		font.SetPointSize(9)

		vbox = wx.BoxSizer(wx.VERTICAL)

		hbox1 = wx.BoxSizer(wx.HORIZONTAL)
		st1 = wx.StaticText(panel, label='Class Name')
		st1.SetFont(font)
		hbox1.Add(st1, flag=wx.RIGHT, border=8)
		tc = wx.TextCtrl(panel)
		hbox1.Add(tc, proportion=1)
		vbox.Add(hbox1, flag=wx.EXPAND|wx.LEFT|wx.RIGHT|wx.TOP, border=10)

		vbox.Add((-1, 10))

		hbox2 = wx.BoxSizer(wx.HORIZONTAL)
		st2 = wx.StaticText(panel, label='Matching Classes')
		st2.SetFont(font)
		hbox2.Add(st2)
		vbox.Add(hbox2, flag=wx.LEFT | wx.TOP, border=10)

		vbox.Add((-1, 10))

		

		#lists
		list_ctrl = wx.ListCtrl(panel, style=wx.LC_REPORT |wx.BORDER_SUNKEN)
		list_ctrl.InsertColumn(0, 'Movies')

		#hbox3.Add(tc2, proportion=1, flag=wx.EXPAND)
		hbox3 = wx.BoxSizer(wx.HORIZONTAL)
		hbox3.Add(list_ctrl)

		vbox.Add(hbox3, flag=wx.LEFT | wx.TOP, border=10)

		vbox.Add((-1, 25))

		hbox4 = wx.BoxSizer(wx.HORIZONTAL)
		cb1 = wx.CheckBox(panel, label='Case Sensitive')
		cb1.SetFont(font)
		hbox4.Add(cb1)
		cb2 = wx.CheckBox(panel, label='Nested Classes')
		cb2.SetFont(font)
		hbox4.Add(cb2, flag=wx.LEFT, border=10)
		cb3 = wx.CheckBox(panel, label='Non-Project classes')
		cb3.SetFont(font)
		hbox4.Add(cb3, flag=wx.LEFT, border=10)
		vbox.Add(hbox4, flag=wx.LEFT, border=10)

		vbox.Add((-1, 25))

		hbox5 = wx.BoxSizer(wx.HORIZONTAL)
		btn1 = wx.Button(panel, label='Ok', size=(70, 30))
		hbox5.Add(btn1)
		btn2 = wx.Button(panel, label='Close', size=(70, 30))
		hbox5.Add(btn2, flag=wx.LEFT|wx.BOTTOM, border=5)
		vbox.Add(hbox5, flag=wx.ALIGN_RIGHT|wx.RIGHT, border=10)

		panel.SetSizer(vbox)
